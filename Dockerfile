FROM docker:1.12.6
RUN apk update && \
    apk add curl bash nodejs python python-dev py-pip build-base openssh-client && \
    pip install docker-compose==1.9.0 && \
    npm install -g triton && \
    curl -L https://raw.githubusercontent.com/joyent/triton-docker-cli/master/triton-docker > /usr/local/bin/triton-docker && \
    ln -s /usr/local/bin/triton-docker /usr/local/bin/triton-compose && \
    ln -s /usr/local/bin/docker /usr/local/bin/triton-docker-helper && \
    ln -s /usr/bin/docker-compose /usr/local/bin/triton-compose-helper && \
    chmod +x /usr/local/bin/triton-* && \
    apk del curl python-dev build-base && \
    rm -r /var/cache/apk/*
